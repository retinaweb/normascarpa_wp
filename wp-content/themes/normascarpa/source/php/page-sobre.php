<?php 
//Template Name: Sobre
?>
<?php get_header(); ?> 

<section>
	<div class="slider">
		<div class="owl-carousel owl-banner owl-theme">
			<?php if(have_rows('banner')): while(have_rows('banner')) : the_row(); ?>
				<div class="item wow fadeIn" data-wow-duration="3s" data-wow-delay="0.8s" style="background: url('<?php echo the_sub_field('imagem'); ?>')no-repeat center center; background-size: cover; position: relative;">
					<div class="container">
						<h2 class="wow fadeInDown" data-wow-duration="3s" data-wow-delay="1.1s"><?php the_sub_field('texto'); ?></h2>
						<a class="wow fadeInDown" data-wow-duration="3s" data-wow-delay="1.5s" href="<?php echo the_sub_field('link') ?>"><?php echo the_sub_field('texto_do_botao'); ?></a>
					</div>
				</div>
			<?php endwhile; endif; ?>
		</div> <!-- owl-banner -->
	</div> <!-- slider -->
</section>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<section>
		<div class="sobre">
			<div class="container">
				<div class="titulo">Norma Scarpa</div>
				<div class="sub-t">Conheça minha história</div>
				<div class="video">
					<?php echo the_field('video'); ?>
				</div>
				<div class="row no-gutters">
					<div class="item">
						<?php echo the_field('texto_esquerda'); ?>
					</div>
					<div class="item">
						<?php echo the_field('texto_direita'); ?>
					</div>
				</div>  <!-- row -->
			</div> <!-- container -->
		</div> <!-- sobre -->
	</section>

<?php endwhile; else: ?>
<?php endif; ?>

<section>
	<div class="conheca-cursos">
		<h2 class="invisivel">Cursos</h2>
		<div class="row no-gutters">
			<!-- 			<div class="titulo">Conheça nossos cursos</div> -->
			<div class="item">
				<div class="container">
					<h3>Cuticulagem</h3>
					<p>
						O aluno desenvolve suas habilidades através do
						aprendizado tendo uma sólida base para aplicar em prática, e recebendo dicas essenciais como
						técnicas de higienização e esterilização dos
						instrumentos de trabalho.
					</p>
					<a href="<?php echo home_url(); ?>/cursos/teste">Conheça nossos cursos</a>
				</div>
			</div> <!-- item -->
			<div class="item">
				<div class="container">
					<h3>Cuticulagem</h3>
					<p>
						O aluno desenvolve suas habilidades através do
						aprendizado tendo uma sólida base para aplicar em prática, e recebendo dicas essenciais como
						técnicas de higienização e esterilização dos
						instrumentos de trabalho.
					</p>
					<a href="<?php echo home_url(); ?>/cursos/teste">Conheça nossos cursos</a>
				</div> <!-- container -->
			</div> <!-- item -->
		</div> <!-- row -->
	</div> <!-- conheca-cursos -->
</section>



<section>
	<div class="fb-ig">
		<div class="row no-gutters">
			<a  href="https://www.instagram.com/norma_scarpa/?hl=pt-br" target="_blank" class="ig">
				<i class="fab fa-instagram"></i> Siga meu instagram
			</a>
			<a  href="https://www.facebook.com/normascarp/" target="_blank" class="fb">
				<i class="fab fa-facebook-square"></i> Me acompanhe no facebook
			</a>
		</div> <!-- row -->
	</div> <!-- fb-ig -->
</section>

<section>
	<div class="fotos-ig">
		<div class="row no-gutters">
			<div class="item"><img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/ig-1.jpg"></div>
			<div class="item"><img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/ig-2.jpg"></div>
			<div class="item"><img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/ig-3.jpg"></div>
			<div class="item"><img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/ig-4.jpg"></div>
		</div> <!-- row -->
	</div> <!-- fotos-ig -->
</section>

<script type="text/javascript">
	$('#sobre').addClass('active');
</script>

<script>
	$(document).ready(function(){
		$(".owl-depo").owlCarousel();
	});
	$(document).ready(function(){
		$(".owl-banner").owlCarousel();
	});
</script>

<?php get_footer(); ?>