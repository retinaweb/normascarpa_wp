<?php 
//Template Name: Contato
?>
<?php get_header(); ?> 

<section>
	<div class="slider">
		<div class="owl-carousel owl-banner owl-theme">
			<?php if(have_rows('banner')): while(have_rows('banner')) : the_row(); ?>
				<div class="item wow fadeIn" data-wow-duration="3s" data-wow-delay="0.8s" style="background: url('<?php echo the_sub_field('imagem'); ?>')no-repeat center center; background-size: cover; position: relative;">
					<div class="container">
						<h2 class="wow fadeInDown" data-wow-duration="3s" data-wow-delay="1.1s"><?php the_sub_field('texto'); ?></h2>
						<a class="wow fadeInDown" data-wow-duration="3s" data-wow-delay="1.5s" href="<?php echo the_sub_field('link') ?>"><?php echo the_sub_field('texto_do_botao'); ?></a>
					</div>
				</div>
			<?php endwhile; endif; ?>
		</div> <!-- owl-banner -->
	</div> <!-- slider -->
</section>

<div class="contato">
	<div class="container">
		<h2>Entre em contato</h2>
		<?php echo do_shortcode( '[contact-form-7 id="79" title="Contato"]' ); ?>
	</div>
</div>

<script type="text/javascript">
	$('#contato').addClass('active');
</script>

<?php get_footer(); ?>