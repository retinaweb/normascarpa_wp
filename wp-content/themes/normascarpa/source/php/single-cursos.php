<?php 
//Template Name: Single Cursos
?>
<?php get_header(); ?> 


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<section>
		<div class="video-single">
			<div class="logo">
				<img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo-footer.png">
			</div>
			<div class="video">
				<iframe width="80%" height="450" src="https://www.youtube.com/embed/<?php echo the_field('video_do_curso'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<a href="<?php echo the_field('link_de_compra_do_curso'); ?>">Compre agora<i class="fas fa-shopping-cart"></i><i class="fas fa-angle-right"></i></a>
		</div> <!-- video-single -->
	</section>

	<section>
		<div class="modulos">
			<div class="container">
				<div class="row no-gutters">
					<div class="foto">
						<img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/norma-fade.png">
					</div>
					<div class="texto">
						<?php the_content(); ?>
					</div>
					<h2 class="invisivel"><?php the_title(); ?></h2>

					<?php if(have_rows('modulos')): while(have_rows('modulos')) : the_row(); ?>
						<div class="holder">
							<div class="item">
								<div class="bolinha">

								</div>
								<h3><?php echo the_sub_field('nome'); ?></h3>
							</div>
						</div>
					<?php endwhile; endif; ?>

				</div>  <!-- row -->
			</div> <!-- container -->
		</div> <!-- modulos -->
	</section>

	<section>
		<div class="divisor">

		</div> <!-- divisor -->
	</section>

	<section>
		<div class="comentario">
			<div class="container">
				<div class="row no-gutters">
					<div class="titulo">O que falam do meu trabalho</div>
					<h2 class="invisivel">Videos</h2>

					<?php if(have_rows('depoimentos_cursos')): while(have_rows('depoimentos_cursos')) : the_row(); ?>

						<div class="holder">
							<div class="item">
								<div class="video">
									<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php echo the_sub_field('video'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
								<p>
									“ <?php echo the_sub_field('depoimento'); ?> ”
								</p>
								<div class="nome"><?php echo the_sub_field('autor'); ?></div>
							</div>
						</div>

					<?php endwhile; endif; ?>

				</div>
			</div>
		</div> <!-- comentario -->
	</section>

	<section>
		<div class="material">
			<div class="container">
				<div class="row no-gutters">
					<h2 class="invisivel">Material</h2>
					<div class="titulo">Meu presente para você!</div>
					<div class="sub-t">Material bônus exclusivo</div>

					<?php if(have_rows('materiais')): while(have_rows('materiais')) : the_row(); ?>

						<div class="holder">
							<div class="item">
								<div class="foto">

								</div>
								<div class="desc">
									<?php echo the_sub_field('nome'); ?>
								</div> <!-- des -->
							</div> <!-- item -->
						</div> <!-- holder -->

					<?php endwhile; endif; ?>

				</div> <!-- row -->
			</div> <!-- container -->
			<a class="botao" href="<?php echo the_field('link_de_compra_do_curso'); ?>">Quero comprar agora<i class="fas fa-angle-right"></i></a>
		</div> <!-- material -->
	</section>
<?php endwhile; else: ?>
<?php endif; ?>

<script type="text/javascript">
	$('#cursos').addClass('active');
</script>


<?php get_footer(); ?>