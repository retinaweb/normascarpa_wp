<?php 
//Template Name: Cursos
?>
<?php get_header(); ?> 

<section>
	<div class="slider">
		<div class="owl-carousel owl-banner owl-theme">
			<?php if(have_rows('banner')): while(have_rows('banner')) : the_row(); ?>
				<div class="item wow fadeIn" data-wow-duration="3s" data-wow-delay="0.8s" style="background: url('<?php echo the_sub_field('imagem'); ?>')no-repeat center center; background-size: cover; position: relative;">
					<div class="container">
						<h2 class="wow fadeInDown" data-wow-duration="3s" data-wow-delay="1.1s"><?php the_sub_field('texto'); ?></h2>
						<a class="wow fadeInDown" data-wow-duration="3s" data-wow-delay="1.5s" href="<?php echo the_sub_field('link') ?>"><?php echo the_sub_field('texto_do_botao'); ?></a>
					</div>
				</div>
			<?php endwhile; endif; ?>
		</div> <!-- owl-banner -->
	</div> <!-- slider -->
</section>




<section>
	<div class="servicos">
		<div class="container">
			<div class="row no-gutters">
				<h2>Nossos Cursos</h2>
				<div class="sub">Conheça os cursos oferecidos<br> 
				no Atelier Norma Scarpa e agende o seu horário!</div>

				<?php
				$paged = get_query_var('paged') ? get_query_var('paged') : 1;
				$args = array (
					'post_type' => 'cursos',
					'order' => 'ASC',
					'posts_per_page' => '-1',
					'paged' => $paged
				);
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
					?>

					<div class="holder">
						<div class="item">
							<h3><?php the_title(); ?></h3>
							<div class="desc"><?php the_field('descricao'); ?></div>
						</div>
						<a href="<?php the_permalink(); ?>">Saiba mais</a>
					</div>

				<?php endwhile; wp_reset_query();?>
			</div> <!-- row -->
		</div> <!-- container -->
	</div> <!-- servicos -->
</section>

<section>
	<div class="on-pres">
		<div class="container">
			<div class="titulo">Curso online</div>
			<div class="desc">
				Aperfeiçoamento de Cuticulagem com Corte Contínuo		
			</div>
			<a href="">Inscreva-se aqui</a>
		</div> <!-- container -->
	</div> <!-- on-pres -->
</section>



<section>
	<div class="fb-ig">
		<div class="row no-gutters">
			<a  href="https://www.instagram.com/norma_scarpa/?hl=pt-br" target="_blank" class="ig">
				<i class="fab fa-instagram"></i> Siga meu instagram
			</a>
			<a  href="https://www.facebook.com/normascarp/" target="_blank" class="fb">
				<i class="fab fa-facebook-square"></i> Me acompanhe no facebook
			</a>
		</div> <!-- row -->
	</div> <!-- fb-ig -->
</section>








<script type="text/javascript">
	$('#cursos').addClass('active');
</script>


<?php get_footer(); ?>