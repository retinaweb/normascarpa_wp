<!DOCTYPE html>
<html lang="pt_BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php
        if(is_home()):

            bloginfo('name');

        elseif(is_category()):

            single_cat_title(); echo ' - ' ; bloginfo('name');

        elseif(is_single()):

            single_post_title(); echo ' - ' ; bloginfo('name');

        elseif(is_page()):

            single_post_title(); echo ' - ' ; bloginfo('name');

        else:

            wp_title('',true);

        endif;

        ?>
    </title>
    <meta name="robots" content="index, follow"/>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-16.png" sizes="16x16" type="image/png">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-48.png" sizes="48x48" type="image/png">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-62.png" sizes="62x62" type="image/png">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-192.png" sizes="192x192" type="image/png">

    <!-- Depêndencias -->
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/owl.theme.default.min.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<!--     <script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/jquery.js"></script>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/lightbox.min.css">
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/lightbox-plus-jquery.min.js"></script>
 -->    <style type="text/css">

    <?php 

    $url = get_bloginfo('template_directory') . '/dist/css/style.css';

    $ch = curl_init();

    curl_setopt ($ch, CURLOPT_URL, $url);

    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5); 

    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

    $contents = curl_exec($ch);

    if(curl_errno($ch)):

        echo curl_error($ch);

        $contents = '';

    else:

        curl_close($ch);

    endif;

    if(!is_string($contents) || !strlen($contents)):

        $contents = '';

endif;

$contents = str_replace('../img/', get_bloginfo('template_directory') . '/dist/img/', $contents);

$contents = str_replace('../fonts/', get_bloginfo('template_directory') . '/dist/fonts/', $contents);

echo $contents;
?>
</style>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
</head>
<body>


    <h1 class="logo_text">Norma Scarpa</h1> 

    <header>

        <div class="menu">
            <div class="container">
                <div class="row no-gutters">
                    <div class="logo">
                        <a href="<?php echo home_url(); ?>/"><img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/norma-scarpa-logo.png"></a>
                    </div> <!-- logo -->

                    <div class="itens-menu">
                        <ul>
                            <li><a id="home" href="<?php echo home_url(); ?>/">Home</a></li>
                            <li><a id="sobre" href="<?php echo home_url(); ?>/sobre">Norma Scarpa</a></li>
                            <li><a id="cursos" href="<?php echo home_url(); ?>/cursos">Cursos</a></li>
                            <li><a id="servicos" href="<?php echo home_url(); ?>/servicos">Serviços</a></li>
                            <li><a id="contato" href="<?php echo home_url(); ?>/contato">Contato</a></li>
                        </ul>
                    </div> <!-- itens-menu -->

                    <div class="redes">
                        <a href="https://www.facebook.com/normascarp/" target="_blank"><i class="fab fa-facebook-square"></i></a>
                        <a href="https://www.instagram.com/norma_scarpa/" target="_blank"><i class="fab fa-instagram"></i></a>
                        <a target="_blank" href="https://api.whatsapp.com/send?phone=5519992101984&text=Ol%C3%A1,%20gostaria%20de%20falar%20com%20um%20consultor" target="_blank"><i class="fab fa-whatsapp"></i></a>
                    </div> <!-- redes -->

                    <div class="menu-toggle">
                        <div class="hamburger"></div>
                    </div>  <!-- toogle -->
                </div> <!-- row -->
            </div> <!-- container -->
        </div>  <!-- menu -->

    </header>

