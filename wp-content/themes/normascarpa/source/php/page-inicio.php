<?php 
//Template Name: Inicio
?>
<?php get_header(); ?> 

<section>
	<div class="slider">
		<div class="owl-carousel owl-banner owl-theme">
			<?php if(have_rows('banner')): while(have_rows('banner')) : the_row(); ?>
				<div class="item wow fadeIn" data-wow-duration="3s" data-wow-delay="0.8s" style="background: url('<?php echo the_sub_field('imagem'); ?>')no-repeat center center; background-size: cover; position: relative;">
					<div class="holder">
						<h2 class="wow fadeInDown" data-wow-duration="3s" data-wow-delay="1.1s"><?php the_sub_field('texto'); ?></h2>
						<a class="wow fadeInDown" data-wow-duration="3s" data-wow-delay="1.5s" href="<?php echo the_sub_field('link') ?>"><?php echo the_sub_field('texto_do_botao'); ?></a>
					</div>
				</div>
			<?php endwhile; endif; ?>
		</div> <!-- owl-banner -->
	</div> <!-- slider -->
</section>

<section>
	<div class="sobre-norma">
		<div class="container">
			<div class="row no-gutters">
				<div class="holder">
					<div class="imagem wow fadeIn" data-wow-duration="3s" data-wow-delay="1.1s">

					</div>
				</div> <!-- holder -->
				<div class="texto wow fadeInRight" data-wow-duration="3s" data-wow-delay="1.1s">
					<h2>Norma Scarpa</h2>
					<p>
						Sou Norma Scarpa, manicure há 8 anos, apaixonada por minha profissão e pela autoestima feminina.
						Comecei em um lugar pequeno e simples, cada dia conquistando e fidelizando mais clientes. Fiz cursos na área da beleza e estética, e me encontrei no Alongamento em Fibra de Vidro.
					</p>
					<p>
						Cresci muito com pouco tempo de espaço e precisei me mudar para um local maior que trouxesse conforto para minhas clientes e que acomodasse uma equipe maior para trabalhar ao meu lado.
					</p>
					<a href="<?php echo home_url(); ?>/cursos">Conheça nossos cursos</a>
				</div> <!-- texto -->
			</div> <!-- row -->
		</div> <!-- container -->
	</div> <!-- sobre -->
</section>

<section>
	<div class="conheca-cursos wow fadeIn" data-wow-duration="1.2s">
		<h2 class="invisivel">Cursos</h2>
		<div class="row no-gutters">
			<div class="titulo">Conheça nossos cursos</div>
			<div class="item wow fadeInLeft" data-wow-duration="1.2s">
				<div class="container">
					<h3>Cuticulagem</h3>
					<p>
						O aluno desenvolve suas habilidades através do
						aprendizado tendo uma sólida base para aplicar em prática, e recebendo dicas essenciais como
						técnicas de higienização e esterilização dos
						instrumentos de trabalho.
					</p>
					<a href="<?php echo home_url(); ?>/cursos/teste">Conheça nossos cursos</a>
				</div> <!-- container -->
			</div> <!-- item -->
			<div class="item wow fadeInRight" data-wow-duration="1.2s">
				<div class="container">
					<h3>Fibra de Vidro</h3>
					<p>
						O curso de Alongamento de Fibra de Vidro capacita o aluno no desenvolvimento de suas habilidades
						desde a preparação das unhas, aplicação da fibra, ponto de tensão, curvatura C, 
						lixamento profissional com motor para alinhar a forma e, por fim, a manutenção,
						zelando pelas normas de biossegurança e pela saúde.
					</p>
					<a href="<?php echo home_url(); ?>/cursos/teste">Conheça nossos cursos</a>
				</div> <!-- container -->
			</div> <!-- item -->
		</div> <!-- row -->
	</div> <!-- conheca-cursos -->
</section>

<section>
	<div class="nossos-servicos">
		<div class="container">
			<div class="row no-gutters">
				<h2 class="invisivel">Serviços</h2>
				<div class="titulo wow fadeInUpBig" data-wow-duration="1.2s">Nossos serviços</div>

				<?php
				$paged = get_query_var('paged') ? get_query_var('paged') : 1;
				$args = array (
					'post_type' => 'servicos',
					'order' => 'ASC',
					'posts_per_page' => '-1',
					'paged' => $paged
				);
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
					?>

					<div class="holder">
						<div class="item wow fadeInUpBig" data-wow-duration="1.2s">
							<div class="holder-i">
								<div class="icone">
									<img class="img-fluid" src="<?php echo the_field('icone'); ?>">
								</div>
							</div>
							<div class="info">
								<h3><?php the_title(); ?></h3>
							</div>
							<a href="<?php the_permalink(); ?>">Saiba mais</a>
						</div>
					</div>

				<?php endwhile; wp_reset_query();?>

			</div> <!-- row -->
		</div> <!-- container -->
	</div> <!-- nossos-servicos -->
</section>

<section>
	<div class="bg-mao">
		
	</div>
</section>


<section>
	<div class="bg-depo">
		<div class="container">
			<h2 class="invisivel">Depoimentos</h2>
			<div class="titulo">Depoimentos</div>
			<div class="owl-carousel owl-depo owl-theme">
				<?php if(have_rows('repetidor_1')): while(have_rows('repetidor_1')) : the_row(); ?>

					<div class="item  wow flipInX" data-wow-duration="1.2s">
						<div class="imagem">
							<img class="img-fluid" src="<?php echo the_sub_field('imagem'); ?>">
						</div>
						<div class="texto">
							<p><?php echo the_sub_field('depoimento'); ?></p>
						</div>
						<div class="nome">- <?php echo the_sub_field('nome'); ?></div>
					</div>	

				<?php endwhile; endif; ?>
			</div><!-- /owl-carousel -->
		</div> <!-- container -->
	</div> <!-- bg-depo -->
</section>



<section>
	<div class="fb-ig">
		<div class="row no-gutters">
			<a  href="https://www.instagram.com/norma_scarpa/?hl=pt-br" target="_blank" class="ig">
				<i class="fab fa-instagram"></i> Siga meu instagram
			</a>
			<a  href="https://www.facebook.com/normascarp/" target="_blank" class="fb">
				<i class="fab fa-facebook-square"></i> Me acompanhe no facebook
			</a>
		</div>
	</div> <!-- fb-ig -->
</section>

<section>
	<div class="fotos-ig">
		<div class="row no-gutters">
			<div class="item"><img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/ig-1.jpg"></div>
			<div class="item"><img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/ig-2.jpg"></div>
			<div class="item"><img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/ig-3.jpg"></div>
			<div class="item"><img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/ig-4.jpg"></div>
		</div><!-- row -->
	</div> <!-- fotos-ig -->
</section>


<script type="text/javascript">
	$('#home').addClass('active');
</script>

<script>
	$(document).ready(function(){
		$(".owl-depo").owlCarousel();
	});
	$(document).ready(function(){
		$(".owl-banner").owlCarousel();
	});
</script>

<?php get_footer(); ?>