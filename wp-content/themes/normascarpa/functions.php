<?php


function my_function_admin_bar(){

	return false;

}

add_filter( 'show_admin_bar' , 'my_function_admin_bar');



add_theme_support( 'post-thumbnails', array( 'post'));

// Limite de caracteres

function excerpt($limit) {

	$excerpt = explode(' ', get_the_excerpt(), $limit);

	if (count($excerpt)>=$limit) {

		array_pop($excerpt);

		$excerpt = implode(" ",$excerpt).'...';

	} else {

		$excerpt = implode(" ",$excerpt);

	}

	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);

	return $excerpt;
}
//CUSTOM POST TYPE
function custom_post_type_cursos() {
	register_post_type('cursos', array(
		'label' => 'cursos',
		'description' => 'cursos',
		'public' => true,
		'menu_icon'   => 'dashicons-welcome-learn-more',
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'page',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array('slug' => 'cursos', 'with_front' => true), 
		'query_var' => true,
		'supports' => array('title', 'editor', 'page-attributes','post-formats', 'thumbnail'),
		'menu_position' => 5,
		'taxonomies'  => array( 'category' ),
		'labels' => array (
			'name' => 'Cursos',
			'singular_name' => 'Cursos',
			'menu_name' => 'Cursos',
			'add_new' => 'Adicionar Novo',
			'add_new_item' => 'Adicionar Novo Curso',
			'edit' => 'Editar',
			'edit_item' => 'Editar Curso',
			'new_item' => 'Novo Curso',
			'view' => 'Ver produto',
			'view_item' => 'Ver Curso',
			'search_items' => 'Procurar Curso Realizado',
			'not_found' => 'Nenhum produto Encontrado',
			'not_found_in_trash' => 'Nenhum Curso Encontrado no Lixo',
		),
	));


}
add_action('init', 'custom_post_type_cursos'); 


//CUSTOM POST TYPE
function custom_post_type_servicos() {
	register_post_type('servicos', array(
		'label' => 'servicos',
		'description' => 'servicos',
		'public' => true,
		'menu_icon'   => 'dashicons-admin-generic',
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'page',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array('slug' => 'servicos', 'with_front' => true), 
		'query_var' => true,
		'supports' => array('title', 'editor', 'page-attributes','post-formats', 'thumbnail'),
		'menu_position' => 5,
		'labels' => array (
			'name' => 'Serviços',
			'singular_name' => 'Serviços',
			'menu_name' => 'Serviços',
			'add_new' => 'Adicionar Novo',
			'add_new_item' => 'Adicionar Novo Serviço',
			'edit' => 'Editar',
			'edit_item' => 'Editar Serviço',
			'new_item' => 'Novo Serviço',
			'view' => 'Ver produto',
			'view_item' => 'Ver Serviço',
			'search_items' => 'Procurar Serviço Realizado',
			'not_found' => 'Nenhum produto Encontrado',
			'not_found_in_trash' => 'Nenhum Serviço Encontrado no Lixo',
		),
	));


}
add_action('init', 'custom_post_type_servicos'); 
add_theme_support( 'post-thumbnails' );
add_theme_support('category-thumbnails');



?>