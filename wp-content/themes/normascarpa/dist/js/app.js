(function($){



  $(document).ready(function() {
    var owl = $('.owl-depo');
    owl.owlCarousel({
      margin: 0,
      animateOut: 'fadeOut',
      loop: false,
      center: true,
      autoplay:true,
      autoplayTimeout:2000,
      dots: false,
      nav: false, 
      responsive: {
        0: {
          items: 1
        },
        562: {
          items: 1
        },
        768: {
          items: 1
        },
        1000: { 
          items: 1
        }
      }
    });
  });

  $(document).ready(function() {
    var owl = $('.owl-banner');
    owl.owlCarousel({
      margin: 0,
      dots: false,
      nav: false,
      loop: true,
      autoplay:true,
      autoplayTimeout:3000,
      responsive: {
        0: {
          items: 1
        },
        562: {
          items: 1
        },
        768: {
          items: 1
        },
        1000: { 
          items: 1
        }
      }
    });
  });

  $(document).ready(function() {
    var owl = $('.owl-galeria');
    owl.owlCarousel({
      margin: 0,
      dots: true,
      nav: false,
      loop: true,
      autoplay: true,
      autoplayTimeout: 3000,
      responsive: {
        0: {
          items: 1
        },
        562: {
          items: 1
        },
        768: {
          items: 1
        },
        1000: { 
          items: 1
        }
      }
    });
  });



 //Efeito Menu
 var _rys = jQuery.noConflict();
 _rys("document").ready(function(){
  _rys(window).scroll(function () {
    if (_rys(this).scrollTop() > 30) {
     _rys('.menu').addClass("mostrar");
   } else { 
    _rys('.menu').removeClass("mostrar");
  }
});
});

 $('.menu-toggle').click(function() {
  $('.itens-menu').toggleClass('opening');
  $('.menu').toggleClass('p-0');
  $('.menu .container').toggleClass('p-0');
  $('.menu .container').toggleClass('m-0');
  $(this).toggleClass('open');
})

var valorDaDiv = $(".screen-reader-response").text();    
if(valorDaDiv == ""){
  return false;
} else{
  alert(valorDaDiv);
}  

})(jQuery);  
