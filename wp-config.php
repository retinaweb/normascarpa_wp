<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'retinawe_norma');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J-}?( P2K8#s(9IR<QJJf8wD&qs4LqvyRxrBP>CT.2k@cC%NhgBedg]5F;Hv/,n+');
define('SECURE_AUTH_KEY',  'CK>UWa,^ei{FF K/iD~ 5Fn@z &wYFWmd7las@Rv&C.Egckh.vuQQnOQOg%jFA~N');
define('LOGGED_IN_KEY',    'AS!=%?<j^0RRzq&H(*aJxMP&FvnE/;KP+o}W@W2;p} lo~8M3?tmDxUG4Sz5V=II');
define('NONCE_KEY',        'k$0SXitfMY&gepKK]s$#Hg|2JcJm=#Ga8GRK76G)xD-_uUpkXGU Q5[284][q/>D');
define('AUTH_SALT',        'wMF[KrH8BQ^e-nWphvhH=NGI3tG1CO^m<F[>hBb3wHUkg~Tc1=J58A}!q$.uncLb');
define('SECURE_AUTH_SALT', '2b}tv&ofV/nz)8bT>OXt-](kTlTv{Dy&P2lkYtru,m)?w,*9GLf4w ):Iz:k}C4Y');
define('LOGGED_IN_SALT',   'PE;[k#=jhig8^zeQKEU]BNs}M@Lu*2WjK8$?oK=y>kFS)HokMs=|8=m<(]MhroP!');
define('NONCE_SALT',       'NB{p-`{`<bP&*^yvIwgPoREZ>,D_bhfxRIjZCy;.F&%Epa]:dae/(+|#/:xfp^`g');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

define("WP_SITEURL","http://localhost/normascarpa/");
define("WP_HOME","http://localhost/normascarpa/");
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


